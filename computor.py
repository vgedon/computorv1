#!/usr/bin/python -O
import sys
import re
import math

debug = 0

class bcolors:
    OKBLUE = '\033[1;34m'
    BLUE = '\033[34m'
    OKGREEN = '\033[32m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    BGREEN = '\033[1;32m'
    UNDERLINE = '\033[4m'

#
#
# MATH functions:

def ft_abs(n):
	return ft_sqrt(n * n)

def ft_sqrt(n):
	if n < 0:
		sys.exit("allo!! you really want to do a division by 0!")
	if n == 0 or n == 1 :
		return n
	res = n
	while 1:
		diff = res
		res = 0.5 * (res + n / res)
		if res == diff:
			break

	return res

def get_fraction(n, d):
	sign = n > 0 and d < 0 or n < 0 and d > 0
	n = ft_abs(n)
	d = ft_abs(d)
	if type(n) is int and type(d) is int:
		# equation will be a +- b/d

		a = int(n/d)
		if a == n/d:
			b = n - a * d
			if d > b and b:
				de = d/b
				if int(de) == de:
					d = int(d / b)
					b = int(b / b)
			return a, b, d, sign
	return 0, n, d, sign

# dis = b*b - 4ac
def discriminant(tab):
	dis = (tab[1] * tab[1]) - (4 * tab[0] * tab[2])
	string = "positive" if dis > 0 else "nul" if dis == 0 else "negative"
	res = "one" if dis == 0 else "two"
	plural = "s" if dis != 0 else ""
	if debug:
		print "{}[debug]{} discriminant value={};".format(bcolors.UNDERLINE, bcolors.ENDC, str(dis))
	print "Discriminant is {c1}{s1}{c2}, {c1}{s2}{c2} solution{s3} exist.".format(s1=string, c1=bcolors.BOLD, c2=bcolors.ENDC, s2=res, s3=plural)
	return dis

#
#
# STRING functions

def parse(string):
	string = string.replace(" ", "")
	p = re.compile("(\-?\d+(?:[.,]\d+)?)(?:(?:[*]?([X|x]))(?:\^?(\d[.,]?\d?))?)?|(=)", re.I)
	equal = 0
	res = p.findall(string)
	if not len(res[0]):
		sys.exit("Syntax Error: equation must be like : aX^2 + bX + c = 0")
	for i in xrange(0, len(res)):
		if len(res[i][3]):
			equal = 1
		if equal and len(res[i][0]):
			res[i] = ('-'+res[i][0], res[i][1], res[i][2], res[i][3])
			if res[i][0].startswith("--"):
				res[i] = (res[i][0].replace("--", ""), res[i][1], res[i][2], res[i][3])
	return res

def stringify(f, n, d, s):
	return "{c1}{si1}{fi}{sp1}{si2}{si3}{sp2}{nu1}{nu2}{div}{di}{c2}".format(c1 = bcolors.BGREEN,
											si1=	"-" if s and f else "",
											fi=		f if f else "",
											sp1=	" " if f and n else "",
											si2=	"-" if s and n else "",
											si3=	"+" if not s and f and n else "",
											sp2=	" " if f and n else "",
											nu1=	n if n else "",
											nu2=	"0" if n == 0 and f == 0 else "",
											div=	"/" if n else "",
											di=		d if n else "",
											c2 = bcolors.ENDC)


#
#
# EQUATION SOLVER related functions
# Simple solution
# x= -b/2a

def simple_solution(tab):
	if not tab[0] and not tab[1] and not tab[2]:
		res = "all real"
	elif not tab[2]:
		print "there is no solution"
		return
	else:
		res = float( - tab[1] ) / float( float(2) * float(tab[2]) )
		if debug:
			print "{}[debug]{}type(res):{};".format(bcolors.UNDERLINE, bcolors.ENDC, type(res))
		if float(res) == int(res):
			res = int(res)
	print "One possible solution:\nX = "+bcolors.BGREEN+str(res)+bcolors.ENDC


# Two solutions
# x1 = (-b-sdis)/2a
# x2 = (-b+sdis)/2a

def all_num_is_int(a2, b, sdis):
	f, n, d, s = get_fraction(b-sdis, a)
	if not f:
		return all_num_is_not_int(a2, b, sdis)
	str_neg = "x1 = "+stringify(f, n, d, s)
	f, n, d, s = get_fraction(b+sdis, a2)
	str_neg = "x2 = "+stringify(f, n, d, s)

	return str_neg, str_pos

def all_num_is_not_int(a2, b, sdis):
	str_neg = "x1 = {}{:.6}{}".format(bcolors.BGREEN,(b-sdis)/a2,bcolors.ENDC)
	str_pos = "x2 = {}{:.6}{}".format(bcolors.BGREEN,(b+sdis)/a2,bcolors.ENDC)
	return str_neg, str_pos

# Two solutions complex
# x1 = (-b -i*sdis) / (2a)
# x1 = (-b +i*sdis) / (2a)

def all_num_is_int_complex(a2, b, sdis):
	# if debug:
		# print "{}[debug]{} f1 = {}; n1 = {}; d1 = {}; s1 = {}".format(bcolors.UNDERLINE, bcolors.ENDC, f, n, d, s)
		# print "{}[debug]{} f2 = {}; n2 = {}; d2 = {}; s2 = {}".format(bcolors.UNDERLINE, bcolors.ENDC, f2, n2, d2, s2)

	str_neg = "x1 = {c1}({b} - i * {sdis}) / ({a2}){c2}".format(c1 = bcolors.BGREEN,b = b, sdis = sdis, a2 = a2, c2 = bcolors.ENDC)
	str_pos = "x2 = {c1}({b} + i * {sdis}) / ({a2}){c2}".format(c1 = bcolors.BGREEN,b = b, sdis = sdis, a2 = a2, c2 = bcolors.ENDC)
	return str_neg, str_pos

def multiple_solution(tab, dis):
	a2 = tab[2] * 2
	b = -tab[1]
	if debug:
		print "{}[debug]{} 2a = {} * 2 = {};".format(bcolors.UNDERLINE, bcolors.ENDC, tab[2], a2)
		print "{}[debug]{} b = -{} = {};".format(bcolors.UNDERLINE, bcolors.ENDC, tab[1], b)
	sdis = ft_sqrt(ft_abs(dis))

	if int(sdis) == sdis:
		sdis = int(sdis)
	str_neg, str_pos = ("", "")
	if debug:
		print "{}[debug]{} b={}; sdis={}; a={};".format(bcolors.UNDERLINE, bcolors.ENDC, b, sdis, a2)

	if dis > 0:
		if type(a2) is int and type(b) is int and type(sdis) is int:
			str_neg, str_pos = all_num_is_int(a2, b, sdis)
		else:
			str_neg, str_pos = all_num_is_not_int(a2, b, sdis)
	else:
		str_neg, str_pos = all_num_is_int_complex(a2, b, sdis)

	print "Two solutions :"	
	print str_neg
	print str_pos



	# (b - "i"div(dis)) / a
	# (b + "i"div(dis)) / a

	# if dis < 0: i

def first_degree(r):
	if debug:
		print "{}[debug]{} r={};".format(bcolors.UNDERLINE, bcolors.ENDC, r)
	a = r[1]
	b = -r[0]
	res = "{:.6}".format(float(float(b)/float(a)))
	if float(float(b)/float(a)) == int(float(float(b)/float(a))):
		res = "{}".format(int(float(b)/float(a)))
	if int(b) == b and int(a) == a and b != a:
		f, n, d, s = get_fraction(int(b), int(a))
		res = "{si1}{fi}{sp1}{si2}{si3}{sp2}{nu1}{nu2}{div}{di}".format(si1="-" if s and f else "",
											fi=int(f) if f else "",
											sp1=" " if f and n else "",
											si2="-" if s and n else "",
											si3="+" if not s and f and n else "",
											sp2=" " if f and n else "",
											nu1=int(n) if n else "",
											nu2="0" if n == 0 and f == 0 else "",
											div="/" if n else "",
											di=int(d) if n else "")
	print "One possible solution:\nx = "+bcolors.BGREEN+str(res)+bcolors.ENDC



#
#
# DISPLAY result

def display(r):
	a = r[2]
	b = r[1]
	c = r[0]
	string = "Reduced form : "
	if a:
		if type(a) is int and a > 1:
			string+= bcolors.BOLD+str(a)
		elif type(a) is int and a < 0:
			string += bcolors.BOLD+"-"
		elif type(a) is float:
			string+= bcolors.BOLD+str(a)+bcolors.ENDC
		string += bcolors.BLUE+"x^2"+bcolors.ENDC
	if a and b:
		if b >= 0:
			string += " + "
		else:
			string += " - "
			b = -b
	if b:
		if b < 0 and not a:
			string += "-"
			b = -b
		if b > 1:
			string += bcolors.BOLD+str(b)+bcolors.ENDC
		string += bcolors.BLUE+"x"+bcolors.ENDC
	if (b or a )and c:
		if c >= 0:
			string += " + "
		else:
			string += " - "
			c= -c
	if c:
		string += bcolors.BOLD+str(c)+bcolors.ENDC
	if not a and not b and not c:
		string += bcolors.BOLD+"0"+bcolors.ENDC
	string += " = "+bcolors.BOLD+"0"+bcolors.ENDC
	print string
	print "Polymonial degree is : "+bcolors.BOLD+str((a and 2) or (b and 1) or 0)+bcolors.ENDC

def simplification(equation):
	tab = [0, 0, 0]
	for ope in equation:
		if ope[0] and float(ope[0]):
			if ope[2] and ((float(ope[2]) != int(ope[2])) or (int(ope[2]) > 2 or int(ope[2]) < 0)):
				sys.exit("Syntax error: exponant must be an integer in range [0-2]. found "+ope[2])
			n = float(ope[0])
			if ope[2]:
				if n != int(n):
					tab[int(ope[2])] += n
				else:
					tab[int(ope[2])] += int(n)
			elif not ope[2] and ope[1]:
				if n != int(n):
					tab[1] += n
				else:
					tab[1] += int(n)
			elif not ope[2] and not ope[1]:
				if n != int(n):
					tab[0] += n
				else:
					tab[0] += int(n)
	return tab


#
#
# ENTRY point

def main():
	if len(sys.argv) > 2:
		sys.exit("usage: "+sys.argv[0]+" [equation]")
	elif len(sys.argv) == 2:
		ret = parse(sys.argv[1])
	else:
		try:
			print "input : "
			read = raw_input()
			while len(read) <= 0:
				read = raw_input()
		except EOFError:
			sys.exit("No input")
		ret = parse(read)
	if not ret:
		sys.exit("Syntax error")
	ret = simplification(ret)
	if debug:
		print "{}[debug]{} simplification:{};".format(bcolors.UNDERLINE, bcolors.ENDC, ret)
	display(ret)
	if not ret[2] and ret[1]:
		if debug:
			"{}[debug]{} first degree equation;".format(bcolors.UNDERLINE, bcolors.ENDC)
		first_degree(ret)
		return
	dis = discriminant(ret)
	if dis == 0:
		simple_solution(ret)
	else:
		multiple_solution(ret, dis)

main()
